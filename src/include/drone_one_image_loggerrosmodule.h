#ifndef DRONELOGGERROSMODULE_H
#define DRONELOGGERROSMODULE_H

#include <ros/ros.h>
#include "droneloggerrosbasicmodule.h"
#include "droneloggerrostopicsubscriber.h"

#include "communication_definition.h"

// Drone Telemetry
//Magnetometer and RotationAngles
#include "geometry_msgs/Vector3Stamped.h"
//Battery
#include "droneMsgsROS/battery.h"
//Altitude
#include "droneMsgsROS/droneAltitude.h"
//Ground Speed
#include "droneMsgsROS/vector2Stamped.h"
//Drone Status
#include "droneMsgsROS/droneStatus.h"
// BottomCamera and FrontCamera
#include "sensor_msgs/Image.h"
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <opencv2/core/core.hpp>        // Basic OpenCV structures (cv::Mat, Scalar)
#include <opencv2/highgui/highgui.hpp>  // OpenCV window I/O
#include "xmlfilereader.h"

// Drone driver commands
#include "droneMsgsROS/dronePitchRollCmd.h"
#include "droneMsgsROS/droneDYawCmd.h"
#include "droneMsgsROS/droneDAltitudeCmd.h"
#include "droneMsgsROS/droneCommand.h"
// EKF pose/speed estimates
#include "droneMsgsROS/dronePose.h"
#include "droneMsgsROS/dronePoseStamped.h"
#include "droneMsgsROS/droneSpeeds.h"
#include "droneMsgsROS/droneSpeedsStamped.h"
// Controller control mode and references
#include "droneMsgsROS/droneTrajectoryControllerControlMode.h"
#include "droneMsgsROS/dronePositionTrajectoryRefCommand.h"

#include "droneMsgsROS/obsVector.h"

#include "droneMsgsROS/landmarkVector.h"
#include "droneMsgsROS/obstaclesTwoDim.h"

#include "droneMsgsROS/societyPose.h"

#include "droneMsgsROS/dronePositionRefCommand.h"
#include "droneMsgsROS/dronePositionRefCommandStamped.h"
#include "droneMsgsROS/droneYawRefCommand.h"

#include "droneMsgsROS/droneHLCommand.h"
#include "droneMsgsROS/droneHLCommandAck.h"
#include "droneMsgsROS/droneMissionInfo.h"
#include "droneMsgsROS/droneGoTask.h"


#include <string>
#include <sstream>

class DroneLoggerROSModule : public DroneLoggerROSBasicModule {
public:
    DroneLoggerROSModule( ros::NodeHandle n);
    ~DroneLoggerROSModule();

private:
    //Images
    //Front
    static bool save_front_camera_images;
    static std::string droneFrontCameraCallback(const sensor_msgs::ImageConstPtr &msg);
    //Bottom
    static bool save_bottom_camera_images;
    static std::string droneBottomCameraCallback(const sensor_msgs::ImageConstPtr &msg);
    //Right
    static bool save_right_camera_images;
    static std::string droneRightCameraCallback(const sensor_msgs::ImageConstPtr &msg);
    //Left
    static bool save_left_camera_images;
    static std::string droneLeftCameraCallback(const sensor_msgs::ImageConstPtr &msg);
    //Back
    static bool save_back_camera_images;
    static std::string droneBackCameraCallback(const sensor_msgs::ImageConstPtr &msg);

    //Cameras
    DroneLoggerROSTopicSubscriber_MsgWithHeader<sensor_msgs::ImageConstPtr> front_camera_subscriber;
    DroneLoggerROSTopicSubscriber_MsgWithHeader<sensor_msgs::ImageConstPtr> bottom_camera_subscriber;
    DroneLoggerROSTopicSubscriber_MsgWithHeader<sensor_msgs::ImageConstPtr> right_camera_subscriber;
//    DroneLoggerROSTopicSubscriber_MsgWithHeader<sensor_msgs::ImageConstPtr> left_camera_subscriber;
//    DroneLoggerROSTopicSubscriber_MsgWithHeader<sensor_msgs::ImageConstPtr> back_camera_subscriber;

    // This boolean is added to perform the reading of configuration files outside of the constructors
private:
    bool configuration_files_are_read;
    bool read_configuration_files();
public:
    bool run();
};

#endif // DRONELOGGERROSMODULE_H
